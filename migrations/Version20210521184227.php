<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210521184227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Tworzenie tabel albumow i utworow';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE albums_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE music_tracks_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE albums (id INT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, main_artist VARCHAR(255) NOT NULL, music_type VARCHAR(255) NOT NULL, medium VARCHAR(255), producer VARCHAR(255), year_of_music INT CONSTRAINT valid_year CHECK(year_of_music > 0), created TIMESTAMP(0) DEFAULT NOW(), modified TIMESTAMP(0) DEFAULT NOW(), PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX album_user_id_idx ON albums (user_id)');
        $this->addSql('CREATE INDEX album_created_idx ON albums (created)');
        $this->addSql('CREATE UNIQUE INDEX album_name_main_artist_user_idx ON albums (name, main_artist, user_id)');
        $this->addSql('CREATE TABLE music_tracks (id INT NOT NULL, album_id INT NOT NULL, title VARCHAR(180) NOT NULL, album_num INT CONSTRAINT valid_album_num CHECK(album_num > 0), artist VARCHAR(155) NOT NULL, created TIMESTAMP(0) DEFAULT NOW(), modified TIMESTAMP(0) DEFAULT NOW(), PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX music_tracks_album_id_idx ON music_tracks (album_id)');
        $this->addSql('CREATE INDEX music_tracks_created_idx ON music_tracks (created)');
        $this->addSql('CREATE UNIQUE INDEX music_tracks_title_albumid_idx ON music_tracks (title, album_id)');
        $this->addSql('ALTER TABLE albums ADD CONSTRAINT albums_user_id_fk FOREIGN KEY (user_id) REFERENCES lusers (id)');
        $this->addSql('ALTER TABLE music_tracks ADD CONSTRAINT music_tracks_album_id_fk FOREIGN KEY (album_id) REFERENCES albums (id)');
    }
}
