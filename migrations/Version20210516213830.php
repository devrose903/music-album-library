<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210516213830 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Tworzenie tabel userow wraz z dodaniem admina';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE SEQUENCE lusers_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE lusers (id INT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, created TIMESTAMP(0) DEFAULT NOW(), modified TIMESTAMP(0) DEFAULT NOW(), PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1C553C76F85E0677 ON lusers (username)');
        $this->addSql("INSERT INTO lusers (id, username, roles, password) VALUES (nextval('lusers_id_seq'), 'admin', '[\"ROLE_ADMIN\"]', '\$argon2id\$v=19\$m=65536,t=4,p=1\$dkdIb3RLMDJWekpKN01KSA\$Ylvy/vxI1yYqdpJOQ5qlJTws5kbB6joHml+XeX1EelE')");
        $this->addSql("INSERT INTO lusers (id, username, roles, password) VALUES (nextval('lusers_id_seq'), 'testUser', '[\"ROLE_USER\"]', '\$argon2id\$v=19\$m=65536,t=4,p=1\$d2p3NXRUZ1REQkgwZGxhNg\$ir2O43beSP8xKQ1D8s9morN3SKoTJKo5kLUzHZ/f2t8')");
    }
}
