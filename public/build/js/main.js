$(function () {
    'use strict';

    function getMaxElement(indexValues) {
        return indexValues.length === 0 ? 0 : Math.max.apply(Math, indexValues) + 1;
    }

    function init() {
        $('.add-music-track').on('click', function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();

            let indexValues, nextRowNumber, selectorProto, table;

            indexValues = $.map($(this).parents('form').find('input[name*=\"title\"]'), function (val) {
                return $(val).attr('name').match(/\[(\d+)\]/)[1];
            });

            nextRowNumber = getMaxElement(indexValues);

            selectorProto = $(this).parents('form').find('.music_track_prototype').html();
            table = $(this).parents('form').find('.music-tracks-form').find('.music-track-rows');

            selectorProto = selectorProto.replace(/__name__/g, nextRowNumber);
            table.append(selectorProto);
        });

        $(document).on('click', '.remove-music-track', function (e) {
            e.preventDefault();
            let element = $(this).closest('.music-track-row');
            element.next("hr").remove();
            element.remove();
        });
    }

    $.app_music_track_form = (function () {
        return {
            init: init,
        };
    }());
});