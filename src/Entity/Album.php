<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="albums",
 *      indexes={@ORM\Index(name="album_user_id_idx", columns={"user_id"}), @ORM\Index(name="album_created_idx", columns={"created"})},
 *      uniqueConstraints={@ORM\UniqueConstraint(name="album_name_main_artist_user_idx", columns={"name", "main_artist", "user_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\AlbumRepository")
 * @UniqueEntity(fields={"name", "mainArtist", "user"},
 *     errorPath="name",
 *     message="Już istnieje taki wpis albumu")
 */
class Album
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="To pole nie może być puste")
     */
    private $name;

    /**
     * @ORM\Column(name="main_artist", type="string", nullable=false)
     * @Assert\NotBlank(message="To pole nie może być puste")
     */
    private $mainArtist;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Assert\NotBlank(message="To pole nie może być puste")
     */
    private $musicType;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $medium;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $producer;

    /**
     * @ORM\Column(name="year_of_music", type="integer", columnDefinition="INT CONSTRAINT valid_year CHECK(year_of_music > 0)", nullable=true)
     * @Assert\DateTime(format="Y", message="Wprowadź poprawny rok")
     * @Assert\Positive(message="Wartość musi być wieksza od 0")
     */
    private $yearOfMusic;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="datetime", columnDefinition="TIMESTAMP(0) DEFAULT NOW()")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", columnDefinition="TIMESTAMP(0) DEFAULT NOW()")
     * @Gedmo\Timestampable(on="update")
     */
    private $modified;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MusicTrack", mappedBy="album", orphanRemoval=true, cascade={"persist"})
     * @Assert\Valid
     */
    private $musicTracks;

    public function __construct()
    {
        $this->musicTracks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMainArtist(): ?string
    {
        return $this->mainArtist;
    }

    public function setMainArtist(string $mainArtist): self
    {
        $this->mainArtist = $mainArtist;

        return $this;
    }

    public function getMusicType(): ?string
    {
        return $this->musicType;
    }

    public function setMusicType(string $musicType): self
    {
        $this->musicType = $musicType;

        return $this;
    }

    public function getMedium(): ?string
    {
        return $this->medium;
    }

    public function setMedium(string $medium): self
    {
        $this->medium = $medium;

        return $this;
    }

    public function getProducer(): ?string
    {
        return $this->producer;
    }

    public function setProducer(string $producer): self
    {
        $this->producer = $producer;

        return $this;
    }

    public function getYearOfMusic(): ?\DateTimeInterface
    {
        $date = new \DateTime();
        $date->setDate($this->yearOfMusic, 1, 1);

        return !empty($this->yearOfMusic)? $date : null;
    }

    public function setYearOfMusic($yearOfMusic): self
    {
        $this->yearOfMusic = $yearOfMusic instanceof \DateTime ? (int)$yearOfMusic->format('Y') : null;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|MusicTrack[]
     */
    public function getMusicTracks(): Collection
    {
        return $this->musicTracks;
    }

    public function addMusicTrack(MusicTrack $musicTrack): self
    {
        if (!$this->musicTracks->contains($musicTrack)) {
            $this->musicTracks[] = $musicTrack;
            $musicTrack->setAlbum($this);
        }

        return $this;
    }

    public function removeMusicTrack(MusicTrack $musicTrack): self
    {
        if ($this->musicTracks->contains($musicTrack)) {
            $this->musicTracks->removeElement($musicTrack);
        }

        return $this;
    }
}