<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="music_tracks",
 *      indexes={@ORM\Index(name="music_tracks_album_id_idx", columns={"album_id"}), @ORM\Index(name="music_tracks_created_idx", columns={"created"})},
 *      uniqueConstraints={@ORM\UniqueConstraint(name="music_tracks_title_albumid_idx", columns={"title", "album_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\MusicTrackRepository")
 * @UniqueEntity(fields={"title", "album"},
 *     errorPath="title",
 *     message="Już istnieje taki tytuł. Wpisz inny.")
 */
class MusicTrack
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, nullable=false)
     * @Assert\NotBlank(message="To pole nie może być puste")
     */
    private $title;

    /**
     * @ORM\Column(name="album_num", type="integer", options={"default": 1}, columnDefinition="INT CONSTRAINT valid_album_num CHECK(album_num > 0)")
     * @Assert\NotBlank(message="To pole nie może być puste")
     * @Assert\Positive(message="To pole musi mieć wartość większą od 0")
     */
    private $albumNum;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Album", inversedBy="musicTracks")
     * @ORM\JoinColumn(name="album_id", referencedColumnName="id", nullable=false)
     * @Assert\NotBlank(message="To pole nie może być puste")
     */
    private $album;

    /**
     * @ORM\Column(type="string", length=155, nullable=false)
     * @Assert\NotBlank(message="To pole nie może być puste")
     */
    private $artist;

    /**
     * @ORM\Column(type="datetime", columnDefinition="TIMESTAMP(0) DEFAULT NOW()")
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", columnDefinition="TIMESTAMP(0) DEFAULT NOW()")
     * @Gedmo\Timestampable(on="update")
     */
    private $modified;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAlbumNum(): ?int
    {
        return $this->albumNum;
    }

    public function setAlbumNum(?int $albumNum): self
    {
        $this->albumNum = $albumNum;

        return $this;
    }

    public function getArtist(): ?string
    {
        return $this->artist;
    }

    public function setArtist(?string $artist): self
    {
        $this->artist = $artist;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getModified(): ?\DateTimeInterface
    {
        return $this->modified;
    }

    public function setModified(\DateTimeInterface $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    public function getAlbum(): ?Album
    {
        return $this->album;
    }

    public function setAlbum(?Album $album): self
    {
        $this->album = $album;

        return $this;
    }
}