<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Controller;

use App\Entity\MusicTrack;
use App\Form\MusicTrackType;
use App\Repository\AlbumRepository;
use App\Repository\MusicTrackRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/music-track")
 */
class MusicTrackController extends AbstractController
{
    /**
     * @Route("/{idalbum}/new", name="music_track_new", methods={"GET","POST"}, requirements={"idalbum"="\d+"})
     */
    public function new(Request $request, int $idalbum, AlbumRepository $albumRepository): Response
    {
        $album = $albumRepository->findAlbumByAlbumId($idalbum);

        if (empty($album)) {
            throw new AccessDeniedException();
        }

        $musicTrack = new MusicTrack();
        $musicTrack->setAlbum($album);

        $form = $this->createForm(MusicTrackType::class, $musicTrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($musicTrack);
            $entityManager->flush();

            $this->addFlash('success', 'Poprawnie dodano utwór muzyczny');

            return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
        }

        return $this->render('music_track/new.html.twig', [
            'musicTrack' => $musicTrack,
            'album' => $album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idalbum}/edit/{id}", name="music_track_edit", methods={"GET","POST"}, requirements={"id"="\d+","idalbum"="\d+"})
     */
    public function edit(Request $request, int $idalbum, int $id, AlbumRepository $albumRepository, MusicTrackRepository $musicTrackRepository): Response
    {
        $album = $albumRepository->findAlbumByAlbumId($idalbum);

        if (empty($album)) {
            throw new AccessDeniedException();
        }

        $musicTrack = $musicTrackRepository->findMusicTrackById($id);

        if (empty($musicTrack)) {
            $this->addFlash('error', 'Nie ma takiego utworu');

            return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
        }

        $form = $this->createForm(MusicTrackType::class, $musicTrack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $musicTrackRepository->updateMusicTrack($musicTrack);

            $this->addFlash('success', 'Poprawnie zaktualizowano utwór.');

            return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
        }

        return $this->render('music_track/edit.html.twig', [
            'musicTrack' => $musicTrack,
            'album' => $album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idalbum}/delete/{id}", name="music_track_delete", methods={"POST"}, requirements={"id"="\d+","idalbum"="\d+"})
     */
    public function delete(Request $request, int $idalbum, int $id, AlbumRepository $albumRepository, MusicTrackRepository $musicTrackRepository): Response
    {
        $album = $albumRepository->findAlbumByAlbumId($idalbum);

        if (empty($album)) {
            throw new AccessDeniedException();
        }

        if ($this->isCsrfTokenValid('delete' . $id, $request->request->get('_token'))) {
            $musicTrack = $musicTrackRepository->findMusicTrackById($id);

            if (empty($album)) {
                throw new NotFoundHttpException();
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($musicTrack);
            $entityManager->flush();

            $this->addFlash('success', 'Poprawnie usunięto utwór.');
        }

        return $this->redirectToRoute('album_show', ['id' => $album->getId()]);
    }
}
