<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Controller;

use App\Entity\Album;
use App\Form\AlbumMusicTracksType;
use App\Form\AlbumType;
use App\Repository\AlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/album")
 */
class AlbumController extends AbstractController
{
    /**
     * @Route("/", name="album_index", methods={"GET"})
     */
    public function index(AlbumRepository $albumRepository): Response
    {
        $albums = $albumRepository->findAllAlbumsForIndexList();

        return $this->render('album/index.html.twig', [
            'albums' => $albums,
        ]);
    }

    /**
     * @Route("/show/{id}", name="album_show", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function show(int $id, AlbumRepository $albumRepository): Response
    {
        $album = $albumRepository->findAlbumWithMusicTracksById($id);

        if (empty($album)) {
            $this->addFlash('error', 'Nie ma takiego wpisu');

            return $this->redirectToRoute('album_index');
        }

        return $this->render('album/show.html.twig', [
            'album' => $album,
        ]);
    }

    /**
     * @Route("/new", name="album_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $album = new Album();
        $album->setUser($this->getUser());

        $form = $this->createForm(AlbumMusicTracksType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($album);
            $entityManager->flush();

            $this->addFlash('success', 'Poprawnie dodano wpis.');

            return $this->redirectToRoute('album_index');
        }

        return $this->render('album/new.html.twig', [
            'album' => $album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="album_edit", methods={"GET","POST"}, requirements={"id"="\d+"})
     */
    public function edit(Request $request, int $id, AlbumRepository $albumRepository): Response
    {
        $album = $albumRepository->findAlbumByAlbumId($id);

        if (empty($album)) {
            $this->addFlash('error', 'Nie ma takiego wpisu');

            return $this->redirectToRoute('album_index');
        }

        $form = $this->createForm(AlbumType::class, $album);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $albumRepository->updateAlbum($album);

            $this->addFlash('success', 'Poprawnie zaktualizowano wpis.');

            return $this->redirectToRoute('album_index');
        }

        return $this->render('album/edit.html.twig', [
            'album' => $album,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="album_delete", methods={"POST"}, requirements={"id"="\d+"})
     */
    public function delete(Request $request, int $id, AlbumRepository $albumRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $id, $request->request->get('_token'))) {

            $album = $albumRepository->findAlbumByAlbumId($id);

            if (empty($album)) {
                throw new NotFoundHttpException();
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($album);
            $entityManager->flush();

            $this->addFlash('success', 'Poprawnie usunięto wpis.');
        }

        return $this->redirectToRoute('album_index');
    }
}
