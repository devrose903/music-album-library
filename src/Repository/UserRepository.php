<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Intl\Exception\RuntimeException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Ten użytkownik: "%s" nie jest wspierany', \get_class($user)));
        }

        $user->setPassword($newEncodedPassword);

        $query = $this->_em->createQuery(
            'UPDATE App\Entity\User u SET u.password = :password WHERE u.id = :userid')
            ->setParameter('password', $newEncodedPassword)
            ->setParameter('userid', $user->getId());
        $query->execute();

        $this->_em->clear($user);
    }

    public function findOneUserByUsername(array $data): ?User
    {
        $trimmedUsername = trim($data['username']);
        $username = filter_var($trimmedUsername, FILTER_SANITIZE_STRING);

        if (empty($username)) {
            return null;
        }

        $query = $this->_em->createQuery(
            'SELECT u FROM App\Entity\User u WHERE u.username = :username')
            ->setParameter('username', $username);

        return $query->getOneOrNullResult();
    }
}
