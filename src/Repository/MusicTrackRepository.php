<?php
/**
 * @Author Weronika Wołoszyk
 */

namespace App\Repository;

use App\Entity\MusicTrack;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MusicTrack|null find($id, $lockMode = null, $lockVersion = null)
 * @method MusicTrack|null findOneBy(array $criteria, array $orderBy = null)
 * @method MusicTrack[]    findAll()
 * @method MusicTrack[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MusicTrackRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MusicTrack::class);
    }

    public function findMusicTrackById(int $idMusicTrack)
    {
        $query = $this->_em->createQuery('SELECT mt FROM App\Entity\MusicTrack mt WHERE mt.id = :idMusicTrack')
            ->setParameter('idMusicTrack', $idMusicTrack);

        return $query->getOneOrNullResult();
    }

    public function updateMusicTrack(MusicTrack $musicTrack)
    {
        $query = $this->_em->createQuery(
            'UPDATE App\Entity\MusicTrack mt SET mt.title = :title, mt.artist = :artist, mt.albumNum = :albumNum WHERE mt.id = :id');

        $query->setParameters([
            'id' => $musicTrack->getId(),
            'title' => $musicTrack->getTitle(),
            'artist' => $musicTrack->getArtist(),
            'albumNum' => $musicTrack->getAlbumNum(),
        ]);

        $query->execute();

        $this->_em->clear('App\Entity\MusicTrack');
    }
}
