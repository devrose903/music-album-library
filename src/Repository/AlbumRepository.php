<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Repository;

use App\Entity\Album;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;

/**
 * @method Album|null find($id, $lockMode = null, $lockVersion = null)
 * @method Album|null findOneBy(array $criteria, array $orderBy = null)
 * @method Album[]    findAll()
 * @method Album[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumRepository extends ServiceEntityRepository
{
    private $security;

    public function __construct(ManagerRegistry $registry, Security $security)
    {
        parent::__construct($registry, Album::class);
        $this->security = $security;
    }

    public function findAllAlbumsForIndexList()
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            $query = $this->_em->createQuery('SELECT partial a.{id, name, mainArtist, musicType}, partial u.{id, username} FROM App\Entity\Album a JOIN a.user u ORDER BY a.id DESC');
        } else {
            $query = $this->_em->createQuery('SELECT partial a.{id, name, mainArtist, musicType} FROM App\Entity\Album a WHERE IDENTITY(a.user) = :userid ORDER BY a.id DESC');

            /** @var User $user */
            $user = $this->security->getUser();
            $query->setParameter('userid', $user->getId());
        }

        return $query->getResult();
    }

    public function findAlbumWithMusicTracksById(int $id): ?Album
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $query = $this->_em->createQuery('SELECT a, mt FROM App\Entity\Album a LEFT JOIN a.musicTracks mt WITH mt.album = a.id WHERE a.id = :idAlbum')
                ->setParameter('idAlbum', $id);
        } else {
            $query = $this->_em->createQuery('SELECT a, mt FROM App\Entity\Album a LEFT JOIN a.musicTracks mt WITH mt.album = a.id WHERE a.id = :idAlbum AND IDENTITY(a.user) = :userId');
            $query->setParameters(['userId' => $user->getId(), 'idAlbum' => $id]);
        }

        return $query->getOneOrNullResult();
    }

    public function findAlbumByAlbumId(int $idAlbum): ?Album
    {
        /** @var User $user */
        $user = $this->security->getUser();

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $query = $this->_em->createQuery('SELECT a FROM App\Entity\Album a WHERE a.id = :idAlbum')
                ->setParameter('idAlbum', $idAlbum);
        } else {
            $query = $this->_em->createQuery('SELECT a FROM App\Entity\Album a WHERE a.id = :idAlbum AND a.user = :idUser')
                ->setParameter('idAlbum', $idAlbum)
                ->setParameter('idUser', $user->getId());
        }

        return $query->getOneOrNullResult();
    }

    public function updateAlbum(Album $album)
    {
        $query = $this->_em->createQuery(
            'UPDATE App\Entity\Album a SET a.name = :name, a.mainArtist = :mArtist, a.musicType = :musicType, a.medium = :medium, a.producer = :producer, a.yearOfMusic = :yearOfMusic WHERE a.id = :id');

        $query->setParameters([
            'id' => $album->getId(),
            'name' => $album->getName(),
            'mArtist' => $album->getMainArtist(),
            'musicType' => $album->getMusicType(),
            'medium' => $album->getMedium(),
            'producer' => $album->getProducer(),
            'yearOfMusic' => (int)$album->getYearOfMusic()->format('Y')
        ]);

        $query->execute();

        $this->_em->clear('App\Entity\Album');
    }
}
