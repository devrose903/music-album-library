<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Form;

use App\Entity\MusicTrack;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MusicTrackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('albumNum', NumberType::class, [
                'label' => 'Numer w albumie*',
                'attr' => [
                    'min' => 1,
                    'step' => 1,
                ],
                'scale' => 0,
                'invalid_message' => 'Nieprawidłowa wartość',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'required' => false,
            ])
            ->add('title', TextType::class, [
                'label' => 'Tytuł utworu*',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'required' => false,
            ])
            ->add('artist', TextType::class, [
                'label' => 'Artysta*',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MusicTrack::class,
        ]);
    }
}