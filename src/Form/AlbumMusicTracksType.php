<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Form;

use App\Entity\Album;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumMusicTracksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nazwa*',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'required' => false,
            ])
            ->add('mainArtist', TextType::class, [
                'label' => 'Artysta/Zespół*',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'required' => false,
            ])
            ->add('musicType', ChoiceType::class, [
                'label' => 'Gatunek muzyczny*',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'attr' => [
                    'class' => 'form-select'
                ],
                'choices' => [
                    'Pop' => 'pop',
                    'Rock' => 'rock',
                    'Dance/Electronic/House' => 'electro',
                    'Soundtracki' => 'soundtracks',
                    'Hip Hop/Rap/Trap' => 'hiphop',
                    'Folk/Indie/Wykonawca piosenki autorskiej' => 'singer',
                    'Muzyka klasyczna/ Opera' => 'classical',
                    'R&B' => 'r&b',
                    'Soul/Blues' => 'soul',
                    'Metal' => 'metal',
                    'Inny' => 'other'
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => false,

            ])
            ->add('medium', ChoiceType::class, [
                'label' => 'Nośnik/format',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'choices' => [
                    'Stream' => 'stream',
                    'Winyl' => 'vinyl',
                    'Format cyfrowy (mp3, WMA, FLAC)' => 'digital',
                    'CD' => 'cd',
                    'Inny' => 'other'
                ],
                'attr' => [
                    'class' => 'form-select'
                ],
                'expanded' => false,
                'multiple' => false,
                'required' => false,
            ])
            ->add('producer', TextType::class, [
                'label' => 'Producent',
                'label_attr' => [
                    'class' => 'col-md-3 control-label',
                ],
                'required' => false,
            ])
            ->add('yearOfMusic', DateTimeType::class, [
                'label' => 'Rok wydania',
                'label_attr' => [
                    'class' => 'control-label'
                ],
                'widget' => 'single_text',
                'format' => 'yyyy',
                'invalid_message' => 'Nieprawidłowa wartość daty',
                'html5' => false,
                'required' => false
            ]);

        $builder->add('musicTracks', CollectionType::class, [
            'label' => 'Utwory muzyczne',
            'entry_type' => MusicTrackType::class,
            'entry_options' => ['label' => false],
            'allow_add' => true,
            'allow_delete' => true,
            'by_reference' => false,
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Album::class,
        ]);
    }
}