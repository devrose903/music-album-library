<?php
/**
 * @Author Weronika Wołoszyk
 */
namespace App\Form;

use App\Entity\Album;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nazwa',
                'label_attr' => [
                    'class' => 'col-sm-2 control-label',
                ],
                'required' => false,
            ])
            ->add('mainArtist', TextType::class, [
                'label' => 'Artysta/Zespół',
                'label_attr' => [
                    'class' => 'col-sm-2 control-label',
                ],
                'required' => false,
            ])
            ->add('musicType', ChoiceType::class,
                [
                    'label' => 'Gatunek muzyczny',
                    'attr' => [
                        'class' => 'form-select'
                    ],
                    'choices' => [
                        'Pop' => 'pop',
                        'Rock' => 'rock',
                        'Dance/Electronic/House' => 'electro',
                        'Soundtracki' => 'soundtracks',
                        'Hip Hop/Rap/Trap' => 'hiphop',
                        'Folk/Indie/Wykonawca piosenki autorskiej' => 'singer',
                        'Muzyka klasyczna/ Opera' => 'classical',
                        'R&B' => 'r&b',
                        'Soul/Blues' => 'soul',
                        'Metal' => 'metal',
                        'Inny' => 'other'
                    ],
                    'expanded' => false,
                    'multiple' => false,
                    'label_attr' => [
                        'class' => 'col-sm-2 control-label',
                    ],
                    'required' => false,
                ]
            )
            ->add('medium', ChoiceType::class, [
                'label' => 'Nośnik/format',
                'attr' => [
                    'class' => 'form-select'
                ],
                'choices' => [
                    'Stream' => 'stream',
                    'Winyl' => 'vinyl',
                    'Format cyfrowy (mp3, WMA, FLAC)' => 'digital',
                    'CD' => 'cd',
                    'Inny' => 'other'
                ],
                'expanded' => false,
                'multiple' => false,
                'label_attr' => [
                    'class' => 'col-sm-2 control-label',
                ],
                'required' => false,
            ])
            ->add('producer', TextType::class, [
                'label' => 'Producent',
                'label_attr' => [
                    'class' => 'col-sm-2 control-label',
                ],
                'required' => false,
            ])
            ->add('yearOfMusic', DateTimeType::class, [
                'label' => 'Rok wydania',
                'widget' => 'single_text',
                'label_attr' => ['class' => 'control-label'],
                'format' => 'yyyy',
                'invalid_message' => 'Nieprawidłowa wartość daty',
                'html5' => false,
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Album::class,
        ]);
    }
}
